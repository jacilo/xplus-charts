    // Custom methods

    Object.defineProperty(Array.prototype, 'chunk', {
      value: function (chunkSize) {
        var temporal = [];

        for (var i = 0; i < this.length; i += chunkSize) {
          temporal.push(this.slice(i, i + chunkSize));
        }

        return temporal;
      }
    });

    ////////////////////////////////////////////////////////
    //  CALL ANALYSIS
    ///////////////////////////////////////////////////////
    google.charts.load("current", {
      packages: ["corechart"]
    });
    google.charts.setOnLoadCallback(drawCallAnalysis);

    function drawCallAnalysis() {
      var dataArr = [
        ["Target Calls", 5295, "#FFFF00"],
        ["Planned Calls", 288, "#00B050"],
        ["Unplanned Calls", 489, "#B1A0C7"],
        ["Calls not Made", 5007, "#000000"],
        ["Effective", 164, "#E26B0A"]
      ];

      dataArr.unshift(["Analysis", "Calls", {
        role: "style"
      }]);

      var data = google.visualization.arrayToDataTable(dataArr);

      var view = new google.visualization.DataView(data);
      view.setColumns([
        0,
        1,
        {
          calc: "stringify",
          sourceColumn: 1,
          type: "string",
          role: "annotation"
        },
        2
      ]);

      var viewa = new google.visualization.DataView(view);
      viewa.setRows([0, 1, 2, 3]);
      var viewb = new google.visualization.DataView(view);
      viewb.setRows([4]);

      var options = {
        animation: {
          duration: 1000,
          easing: "linear",
          startup: true
        },
        annotations: {
          textStyle: {
            fontSize: 12
          }
        },
        chartArea: {
          width: "100%"
        },
        title: "",
        seriesType: "bars",
        legend: {
          position: "none"
        },
        hAxis: {
          textStyle: {
            fontSize: 12
          }
        },
        vAxis: {
          textStyle: {
            color: "#000"
          },
          viewWindowMode: "none",
          viewWindow: {
            max: dataArr[1][1],
            min: 0
          },
          gridlines: {
            color: "transparent",
            count: 8
          },
          direction: "-1"
        },
        enableInteractivity: false,
        backgroundColor: 'transparent'
      };

      options.width = 100 * viewa.getNumberOfRows();

      var container = document.getElementById("call_analysis_a");
      var charta = new google.visualization.ColumnChart(container);

      // move annotations
      var observer = new MutationObserver(function () {
        Array.prototype.forEach.call(
          container.getElementsByTagName("text"),
          function (annotation) {
            for (var i = 1; i < dataArr.length; i++) {
              if (i > 0 && i !== dataArr.length - 1) {
                if (
                  parseFloat(annotation.textContent.replace(/,/g, "")) ==
                  dataArr[i][1] &&
                  annotation.getAttribute("text-anchor") === "middle"
                ) {
                  var chartLayout = charta.getChartLayoutInterface();
                  annotation.setAttribute("y", 35);
                }
              }
            }
          }
        );
      });
      observer.observe(container, {
        childList: true,
        subtree: true
      });

      charta.draw(viewa, options);

      options.vAxis.direction = 1;
      options.width = 100 * viewb.getNumberOfRows();
      var chartb = new google.visualization.ComboChart(
        document.getElementById("call_analysis_b")
      );
      chartb.draw(viewb, options);
    }

    ////////////////////////////////////////////////////////
    // REASONS FOR NOT SELLING
    ///////////////////////////////////////////////////////
    google.charts.load("current", {
      packages: ["corechart"]
    });
    google.charts.setOnLoadCallback(drawRFNS);

    function drawRFNS() {
      var dataArray = [
        ["Sufficient Inventory", 51],
        ["No Owner", 14],
        ["Purchase from other Source", 29],
        ["Pending collections", 21],
        ["Stored close", 10],
        ["Pending delivery", 21],
        ["Pricing issue", 2],
        ["Dissatisfied customer", 0],
        ["Bad account", 7]
      ];
      var total = getTotal(dataArray);

      // Adding tooltip column
      for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].push(customTooltip(dataArray[i][0], dataArray[i][1], total));
      }

      // Changing legend
      for (var i = 0; i < dataArray.length; i++) {
        dataArray[i][0] =
          dataArray[i][0] +
          " " +
          dataArray[i][1] +
          " " +
          ((dataArray[i][1] / total) * 100).toFixed(1) +
          "%";
      }

      // Column names
      dataArray.unshift(["Reasons", "Counts and Ranks", "Tooltip"]);

      var data = google.visualization.arrayToDataTable(dataArray);

      // Setting role tooltip
      data.setColumnProperty(2, "role", "tooltip");
      data.setColumnProperty(2, "html", true);

      var options = {
        animation: {
          duration: 1000,
          easing: "out",
          startup: true
        },
        pieHole: 0.4,
        legend: {
          position: "left"
        },
        backgroundColor: "#FFF",
        chartArea: {
          left: "3%",
          top: "3%",
          height: "85%",
          width: "85%"
        },
        pieSliceText: "percentage",
        pieSliceTextStyle: {
          fontSize: 12
        },
        tooltip: {
          isHtml: true
        },
        colors: ['#0487A6', '#EF4F31', '#F3764A', '#8EB83E', '#CF89BB', '#69C3E3', '#A83623', '#4DA580', '#C9222B'],
        backgroundColor: 'transparent'
      };

      var chart = new google.visualization.PieChart(
        document.getElementById("reasons_for_not_selling")
      );
      chart.draw(data, options);

      function customTooltip(name, value, total) {
        return (
          name +
          "<br/><b>" +
          value +
          " (" +
          ((value / total) * 100).toFixed(1) +
          "%)</b>"
        );
      }

      function getTotal(dataArray) {
        var total = 0;
        for (var i = 0; i < dataArray.length; i++) {
          total += dataArray[i][1];
        }
        return total;
      }
    }

    ////////////////////////////////////////////////////////
    //  BOOKED VOLUME AND BUYING ACCOUNTS
    ///////////////////////////////////////////////////////
    google.charts.load("current", {
      packages: ["corechart", "bar"]
    });
    google.charts.setOnLoadCallback(drawBVandBA);

    function drawBVandBA() {
      var dataArr = [
        ["Month", "Book Orders", "Buying Accounts", "Throughput"],
        ["January", 0, 0, 0],
        ["February", 0, 0, 0],
        ["March", 0, 0, 0],
        ["April", 0, 0, 0],
        ["May", 1.63, 55, 201.25],
        ["June", 66.22, 551, 610.28],
        ["July", 203.18, 943, 1],
        ["August", 13.15, 155, 424.3],
        ["September", 0, 0, 0],
        ["October", 0, 0, 0],
        ["November", 0, 0, 0],
        ["December", 0, 0, 0]
      ];

      var data = google.visualization.arrayToDataTable(dataArr);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1, {
        calc: "stringify",
        sourceColumn: 1,
        type: "string",
        role: "annotation"
      }, 2, {
        calc: "stringify",
        sourceColumn: 2,
        type: "string",
        role: "annotation"
      }]);

      var options = {
        animation: {
          duration: 1000,
          easing: "out",
          startup: true
        },
        annotations: {
          textStyle: {
            fontSize: 11,
            color: "black",
            auraColor: "none"
          }
        },
        chartArea: {
          right: "6%",
          width: "80%"
        },
        vAxes: {
          0: {
            viewWindowMode: "explicit",
            gridlines: {
              color: "transparent",
              count: 8
            }
          },
          1: {
            gridlines: {
              color: "transparent",
              count: 7
            }
          }
        },
        series: {
          0: {
            annotations: {
              stem: {
                color: 'transparent',
                length: 0
              }
            },
            targetAxisIndex: 1,
            color: "#41ACB2",
            type: "bars"
          },
          1: {
            annotations: {
              stem: {
                length: 7
              }
            },
            type: "line",
            targetAxisIndex: 0,
            color: "#F47A01",
            pointShape: "square",
            pointsVisible: true
          }
        },
        legend: {
          position: "top",
          alignment: "start"
        },
        backgroundColor: 'transparent',
        tooltip: {
          textStyle: {
            color: '#000000',
            fontSize: 15
          }
        }
      };

      var container = document.getElementById("bv_and_ba");
      var chart = new google.visualization.ColumnChart(container);

      var annotations = [];
      var chartLayout;
      var annotationsReady = false;

      // get annotation text for element identification
      google.visualization.events.addListener(chart, 'ready', function () {
        chartLayout = chart.getChartLayoutInterface();
        for (var i = 0; i < view.getNumberOfRows(); i++) {
          annotations.push(view.getValue(i, 2));
        }
      });

      // wait until annotations are ready to move
      google.visualization.events.addListener(chart, 'animationfinish', function () {
        annotationsReady = true;
        moveAnnotations();
      });

      function moveAnnotations() {
        // ensure annotations are ready
        if (!annotationsReady) {
          return;
        }

        // move annotations
        Array.prototype.forEach.call(container.getElementsByTagName('text'), function (annotation) {
          // exclude other labels
          if (annotation.getAttribute('text-anchor') === 'middle') {
            var annotationTop;
            var barBounds;

            // check if annotation to be moved
            var rowIndex = annotations.indexOf(annotation.textContent);
            if (rowIndex > -1) {
              // get bar bounds
              barBounds = chartLayout.getBoundingBox('bar#0#' + rowIndex);
              // calculate annotation position
              annotationTop = barBounds.top + (barBounds.height / 2) + (annotation.getBBox().height / 2);
              // move annotation
              annotation.setAttribute('y', annotationTop);
            }
          }
        });
      }

      // prevent chart from moving annotations back to original position
      var observer = new MutationObserver(moveAnnotations);
      observer.observe(container, {
        childList: true,
        subtree: true
      });

      chart.draw(view, options);

      // Legend table
      (function () {
        var table =
          '<table class"table" style="width: 95%;"><tr><th class="months"></th>';
        for (var i = 0; i < dataArr.length; i++) {
          if (i > 0) {
            table += "<th class='months'>" + dataArr[i][0] + "</th>";
          }
        }
        table += "</tr>";
        table +=
          '<tr><td class="legends">' +
          dataArr[0][1] +
          "</td>";
        for (var i = 0; i < dataArr.length; i++) {
          if (i > 0) {
            table += "<td>" + dataArr[i][1] + "</td>";
          }
        }

        table +=
          '<tr><td class="legends">' +
          dataArr[0][2] +
          "</td>";
        for (var i = 0; i < dataArr.length; i++) {
          if (i > 0) {
            table += "<td>" + dataArr[i][2] + "</td>";
          }
        }

        table += '<tr><td class="legends">' + dataArr[0][3] + "</td>";
        for (var i = 0; i < dataArr.length; i++) {
          if (i > 0) {
            table += "<td>" + dataArr[i][3] + "</td>";
          }
        }

        table += "</tr></table>";
        $(".bv_and_ba_legend")
          .empty()
          .append(table);
      })();
    }

    ////////////////////////////////////////////////////////
    // DSR Performance View
    ///////////////////////////////////////////////////////
    google.charts.load("current", {
      packages: ["corechart"]
    });
    google.charts.setOnLoadCallback(drawDSRPerformance);

    function drawDSRPerformance() {
      var dataArr = [
        ["Person", "Booked Volume", "Number of calls", "Effective calls"],
        ["Albert", 1, 51, 11],
        ["Almario", 21.6, 20, 1],
        ["Anthony", 388.8, 41, 6],
        ["Bataan", 0, 0, 0],
        ["Charles", 0, 13, 0],
        ["Daryl", 0, 0, 0],
        ["Dharil", 571.2, 30, 8],
        ["Domjan", 0, 0, 0],
        ["East Batangas", 0, 0, 0],
        ["Edwin", 626, 39, 11],
        ["Erwin", 664, 59, 15],
        ["Francis Xavier", 31.2, 7, 1],
        ["GIL", 1, 24, 9],
        ["In-house", 0, 0, 0],
        ["Islands", 0, 0, 0],
        ["Jan Patrick Lawrence", 362.4, 36, 9],
        ["Jay", 512.8, 50, 9],
        ["Jerome", 336, 51, 8],
        ["Jobert", 541.2, 47, 15],
        ["Joel", 0, 6, 0],
        ["Jonathan", 0, 0, 0],
        ["Joseph", 128, 15, 4],
        ["Joseph N", 359.36, 26, 5],
        ["Key Accounts - E Luzon", 0, 0, 0],
        ["Key Accounts - N Luzon", 0, 0, 0],
        ["Key Accounts - S Luzon", 0, 0, 0],
        ["Key Accounts - S Metro", 0, 0, 0],
        ["Leandro", 0, 29, 0],
        ["Lower Laguna", 0, 0, 0],
        ["Lysander", 48, 15, 1],
        ["Mark Joseph", 2, 35, 18],
        ["Marvin", 808.8, 25, 3],
        ["NELSON", 0, 12, 0],
        ["Nicolas", 0, 23, 0],
        ["Paul Vernon", 164.48, 25, 5],
        ["Ray", 0, 0, 0],
        ["Renato", 930.4, 54, 11],
        ["Ryan", 796.8, 44, 14],
        ["TBH", 0, 0, 0],
        ["xplus_jr", 0, 0, 0]
      ];

      var setCol = [
        0,
        1,
        {
          calc: "stringify",
          sourceColumn: 1,
          type: "string",
          role: "annotation"
        },
        2,
        {
          calc: "stringify",
          sourceColumn: 2,
          type: "string",
          role: "annotation"
        },
        3,
        {
          calc: "stringify",
          sourceColumn: 3,
          type: "string",
          role: "annotation"
        }
      ];

      var options = {
        explorer: {
          axis: 'horizontal',
          keepInBounds: true
        },
        animation: {
          duration: 1000,
          easing: "out",
          startup: true
        },
        hAxis: {
          gridlines: {
            count: 5
          }
        },
        vAxes: {
          0: {
            viewWindowMode: "explicit",
            gridlines: {
              color: "transparent",
              count: 10
            }
          },
          1: {
            gridlines: {
              color: "transparent",
              count: 7
            }
          },
          2: {
            gridlines: {
              color: "transparent",
              count: 7
            }
          }
        },
        seriesType: "bars",
        series: {
          0: {
            targetAxisIndex: 0,
            color: "#FFE64C"
          },
          1: {
            annotations: {
              stem: {
                length: 8
              }
            },
            type: "line",
            targetAxisIndex: 1,
            color: "#41ACB2",
            pointShape: "square",
            pointsVisible: true
          },
          2: {
            annotations: {
              stem: {
                length: 4
              }
            },
            type: "line",
            targetAxisIndex: 1,
            color: "#EF4F31",
            pointShape: "square",
            pointsVisible: true
          }
        },
        annotations: {
          textStyle: {
            fontSize: 12,
            auraColor: "none",
            color: "black"
          }
        },
        chartArea: {
          height: "385px",
          top: 30
        },
        legend: {
          position: "bottom",
          alignment: "center"
        },
        backgroundColor: 'transparent',
        tooltip: {
          textStyle: {
            color: '#000000',
            fontSize: 15
          }
        }
      };

      var container = document.getElementById("dsr_performance");
      var chart = new google.visualization.ComboChart(container);

      // move annotations
      var observer = new MutationObserver(function () {
        Array.prototype.forEach.call(
          container.getElementsByTagName("text"),
          function (annotation) {
            if (
              annotation.getAttribute("text-anchor") === "middle" &&
              annotation.getAttribute("fill") === "#404040"
            ) {
              var chartLayout = chart.getChartLayoutInterface();
              annotation.setAttribute(
                "y",
                chartLayout.getYLocation(0) -
                parseInt(annotation.getAttribute("font-size")) / 2
              );
            }
          }
        );
      });

      observer.observe(container, {
        childList: true,
        subtree: true
      });

      EnablePagination(5, document.getElementById('prevButton'), document.getElementById('nextButton'));

      function EnablePagination(ps, prevButton, nextButton) {
        var pageSize = ps;
        var header = [];
        var dataRows = [];

        (function getDataRowsAndHeader() {
          for (var i = 0; i < dataArr.length; i++) {
            if (i === 0) {
              header = dataArr[i];
            } else {
              dataRows.push(dataArr[i]);
            }
          }
        })();

        var dataRowChunks = [];

        dataRowChunks = dataRows.chunk(pageSize);
        for (var i = 0; i < dataRowChunks.length; i++) {
          dataRowChunks[i].unshift(header);
        }

        var currentPage = -1;
        var paginate = function (next) {
          next ? currentPage++ : currentPage--;

          if (currentPage <= -1 || currentPage >= dataRowChunks.length) {
            next ? currentPage-- : currentPage++;
            $('.page').text((currentPage + 1) + '/' + dataRowChunks.length);
            return;
          }

          $('.page').text((currentPage + 1) + '/' + dataRowChunks.length);

          var data = google.visualization.arrayToDataTable(dataRowChunks[currentPage]);
          var view = new google.visualization.DataView(data);

          view.setColumns(setCol);

          chart.draw(view, options);
        }

        prevButton.onclick = function (e) {
          paginate(false);
          e.preventDefault();
        };
        nextButton.onclick = function (e) {
          paginate(true);
          e.preventDefault();
        };
        paginate(true);
      }
    }

    $(window).resize(function () {
      drawDSRPerformance();
      drawBVandBA();
      drawCallAnalysis();
      drawRFNS();
    });

    // setInterval(() => {
    //   drawDSRPerformance();
    //   drawBVandBA();
    //   drawCallAnalysis();
    //   drawRFNS();
    // }, 3000);